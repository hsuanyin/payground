import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Layout from '../components/layout';

class TestLayout extends Component {
    render() {
        return (
            <Layout title="Test Layout Page | Playground">
                <Typography variant="display2">I'm the second one</Typography>
            </Layout>
        );
    }
}

TestLayout.propTypes = {

};

export default TestLayout;