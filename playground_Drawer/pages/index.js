import React, { Component } from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Layout from "../components/layout";

class Index extends Component {
  render() {
    return (
      <React.Fragment>
          <Layout title="Home | Playground">
            <Typography variant="display4">Testing the paper!</Typography>
          </Layout>
      </React.Fragment>
    );
  }
}

Index.propTypes = {};

export default Index;
