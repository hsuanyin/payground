import React, { Component } from "react";
import PropTypes from "prop-types";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";

const drawerWidth = 240;

const styles = theme => ({
  drawerPaper: {
    width: drawerWidth
  },
  "toggleButton-open": {
    marginLeft: drawerWidth
  },
  "toggleButton-close": {
    marginLeft: 0
  }
});

class Navigation extends Component {
  state = { open: true };

  handleOpenClose = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;
    return (
      <React.Fragment>
        <Drawer
          variant="persistent"
          open={open}
          anchor="left"
          classes={{ paper: classes.drawerPaper }}
        >
          <Link href="/">
            <Button>Home</Button>
          </Link>
          <Link href="/testLayout">
            <Button>Test Layout</Button>
          </Link>
        </Drawer>
        <Button
          classes={{ root: classes[`toggleButton-${open ? "open" : "close"}`] }}
          onClick={this.handleOpenClose}
          variant="fab"
        >
          {open ? "X" : ">"}
        </Button>
      </React.Fragment>
    );
  }
}

Navigation.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Navigation);
