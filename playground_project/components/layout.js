import React, { Component } from "react";
import PropTypes from "prop-types";
import Navigation from "./navigation";
import Content from "./content";
import Head from "next/head";

class Layout extends Component {
  render() {
    const { children, title } = this.props;
    return (
      <React.Fragment>
        <Head>
          <title>{title}</title>
          <meta charSet="utf-8" />
        </Head>
        <Navigation />
        <Content>{children}</Content>
      </React.Fragment>
    );
  }
}

Layout.propTypes = {};

export default Layout;
