import React, { Component } from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";

const drawerWidth = 240;

const styles = theme => ({
  root: {
    marginLeft: drawerWidth
  }
});

class Content extends Component {
  render() {
    const { children, classes } = this.props;
    return (
      <div class={classes.root}>
        <Grid container justify="center">
          <Grid item xs={8}>
            <Paper>{children}</Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

Content.propTypes = {};

export default withStyles(styles)(Content);
